from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpRequest
from django.views.generic import FormView, UpdateView

from .. import forms, models


class DeveloperRequestView(LoginRequiredMixin, FormView):
    template_name = 'bid_main/developer_request.html'
    form_class = forms.DeveloperAccessRequestForm
    success_url = '/'

    def form_valid(self, form: forms.DeveloperAccessRequestVerificationForm):
        request_model = form.save(commit=False)
        request_model.user = self.request.user
        request_model.save()
        return super().form_valid(form)


class DeveloperRequestVerificationView(LoginRequiredMixin, UpdateView):
    template_name = 'bid_main/developer_request_verification.html'
    fields = ['sponsor_agreed']
    model = models.DeveloperAccessRequest
    success_url = '/'

    def dispatch(self, request: HttpRequest, *args, **kwargs):
        request_model = models.DeveloperAccessRequest.objects\
            .get(pk=kwargs.get('pk'))
        if request.user != request_model.user:
            return self.handle_no_permission()
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form: forms.DeveloperAccessRequestVerificationForm):
        request_model = form.save(commit=False)
        request_model.user = self.request.user
        request_model.save()
        return super().form_valid(form)
